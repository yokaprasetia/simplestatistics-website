<?php
include "../koneksi.php";

$referensi_judul = $_GET['key'];

$result = mysqli_query($koneksi, "SELECT * FROM referensi WHERE referensi_judul = '$referensi_judul'");
$row = mysqli_fetch_assoc($result);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/update.css">
    <title>UPDATE</title>
</head>

<body>
    <div class="form">
        <h1>Referensi Form</h1>
        <form action="prosesUpdateReferensi.php" method="POST">

            <label for="href">Referensi Href</label>
            <input type="text" id="href" name="referensi_href" placeholder=" Online Link" value="<?php echo $row['referensi_href']; ?>" readonly>
            <label for="id">ID</label>
            <input type="text" id="id" name="id" placeholder="Recent ID : anareg, statmat, oop, komstat, sig, pbw" value="<?php echo $row['id']; ?>" required>
            <label for="judul">Referensi Judul</label>
            <input type="text" id="judul" name="referensi_judul" placeholder="Judul dari Konten Anda" value="<?php echo $row['referensi_judul']; ?>" required>
            <ul>
                <li><button type="submit" name="update">Update</button></li>
                <li><button type="button" name="kembali" onclick="window.history.back()">Kembali</button></li>
            </ul>
        </form>
    </div>
</body>

</html>