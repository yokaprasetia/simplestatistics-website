<?php
include "../koneksi.php";
if (isset($_POST['prosesAdding'])) {

    $materi_href = $_POST['materi_href'];
    $id = $_POST['id'];
    $materi_judul = $_POST['materi_judul'];

    $query = mysqli_query($koneksi, "SELECT * FROM materi WHERE materi_href = '$materi_href'");
    $cek = mysqli_num_rows($query);

    // Error Handling
    if ($cek == 1) {
        echo "
            <script>
            alert('Gagal Menambah Data! Informasi telah digunakan...');
            document.location = 'DevMateri.php';
            </script>
            ";
    } else {
        mysqli_query($koneksi, "INSERT INTO materi VALUES('$materi_href', '$id', '$materi_judul')");

        echo "
            <script>
                alert('Informasi Berhasil Ditambahkan!');
                document.location = 'DevMateri.php';
            </script>
            ";
    }
}
?>


<?php
session_start();
if (isset($_SESSION['username'])) {
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/devCreate.css">
        <link rel="icon" href="../img/icon-dev.png">
        <title>MY DEV - Materi Form</title>
    </head>

    <body>
        <div class="main">
            <div class="navbar">
                <ul>
                    <li><a href="DevMainMaterial.php">MAIN MATERIAL</a></li>
                    <li><a class="active" href="DevMateri.php">MATERI</a></li>
                    <li><a href="DevVideo.php">VIDEO</a></li>
                    <li><a href="DevReferensi.php">REFERENSI</a></li>
                    <li><a href="DevOther.php">OTHER</a></li>
                </ul>
            </div>
            <div class="form">
                <div class="border">
                    <h1>Materi Form</h1>
                    <form action="DevMateri.php" method="POST">
                        <label for="href">Materi Href</label>
                        <input type="text" id="href" name="materi_href" placeholder=" Online Link" required>
                        <label for="id">ID</label>
                        <input type="text" id="id" name="id" placeholder="Recent ID : anareg, statmat, oop, komstat, sig, pbw" required>
                        <label for="judul">Materi Judul</label>
                        <input type="text" id="judul" name="materi_judul" placeholder="Judul dari Konten Anda" required>
                        <input type="submit" value="ADD DATA" name="prosesAdding">
                    </form>
                </div>
            </div>
            <div class="logout">
                <div>
                    <a href="DevLogOut.php" onclick="return confirm('Apakah anda ingin Sign Out ?')"><img src="../img/dev-icon1.png"></a>
                    <p>Sign Out</p>
                </div>
            </div>
            <a class="back-to-menu" href="DevChoice.php">Back to Menu</a>
        </div>
    </body>

    </html>

<?php
} else {
    echo "
        <script>
            alert('Maaf! Login terlebih dahulu..');
            document.location = 'DevLogin.php';
        </script>
        ";
}

?>