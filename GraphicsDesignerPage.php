<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="img/all-faviconWeb.png">
    <title>Simple Statistik - Graphics Designer</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        body {
            background-image: url(img/bg-materi.png);
            background-size: cover;
            background-attachment: fixed;
        }
        .main {
            height: 100vh;
            width: 100vw;
            display: flex;
            justify-content: center;
            align-items: center;
            color: white;
        }
        .center {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .center h1 {
            margin-bottom: 20px;
        }
        .center ul {
            list-style: none;
            text-align: center
        }
        .center ul li {
            text-transform: uppercase;
            padding: 5px;
        }
    </style>
</head>
<body>
    <div class="main">
        <div class="center">
            <h1>GRAPHICS DESIGNER DAY</h1>
            <ul>
                <li>Corel Draw</li>
                <li>Adobe Ilustrator</li>
                <li>Inkscape</li>
                <li>Canva Online</li>
                <li>Paint</li>
                <li>3D Object</li>
            </ul>
        </div>
    </div>
</body>
</html>