<?php
session_start();
include "koneksi.php";

if (isset($_POST['prosesSignUp'])) {

    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $konfirmasi = $_POST['konfirmasi'];

    //cek ketersediaan data di database
    $query = mysqli_query($koneksi, "SELECT* FROM user WHERE email = '$email'");
    $cek = mysqli_num_rows($query);

    //Cek panjang password
    $panjang_password = strlen('$password');

    // ERROR HANDLING
    if ($password !== $konfirmasi) {
        echo "
        <script>
            alert('Gagal dibuat! Konfirmasi password tidak sesuai...');
        </script>
        ";
    } elseif ($cek == 1) {
        echo "
            <script>
            alert('Gagal dibuat! Email telah digunakan...')
            document.location = 'SignUpPage.php';
            </script>
            ";
    } elseif (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email)) {
        echo "
            <script>
            alert('Gagal dibuat! Pastikan Alamat Email adalah benar...')
            document.location = 'SignUpPage.php';
            </script>
            ";
    } elseif (strlen($_POST['password']) < 8) {
        echo "
            <script>
            alert('Gagal dibuat! Password minimal 8 digit...')
            document.location = 'SignUpPage.php';
            </script>
            ";
    } elseif (!preg_match('/^([a-zA-Z]+[0-9]+)*([0-9]+[a-zA-Z]+)*$/', $password)) {
        echo "
            <script>
            alert('Gagal dibuat! Password kombinasi angka dan huruf...')
            document.location = 'SignUpPage.php';
            </script>
            ";
    } else {
        $password = password_hash($password, PASSWORD_DEFAULT);
        mysqli_query($koneksi, "INSERT INTO user VALUES('$first_name', '$last_name', '$email', '$password')");

        echo "
            <script>
                alert('Akun Berhasil Dibuat!...')
                document.location = 'SignInPage.php';
            </script>
            ";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/signPagess.css">
    <link rel="icon" href="img/all-faviconWeb.png">
    <title>Simple Statistik - Sign Up</title>
</head>

<body>
    <div class="content">

        <div class="sign-up">
            <div class="form">
                <div class="center">
                    <h1>Hello Statistician!</h1>
                    <p class="sub">Sign Up to Get Started, okay?</p>
                    <hr>
                    <form action="SignUpPage.php" method="POST">
                        <input type="text" name="first_name" placeholder="First Name" required>
                        <input type="text" name="last_name" placeholder="Last Name" required>
                        <input type="email" name="email" placeholder="E-mail" required>
                        <input type="password" name="password" placeholder="Password" required>
                        <input type="password" name="konfirmasi" placeholder="Konfirmasi Password" required>
                        <p class="pass">Password must be at least 8 characters</p>
                        <button type="submit" name="prosesSignUp">Sign Up</button>
                    </form>
                    <p class="link">Already a member? <a href="SignInPage.php">Sign In</a></p>
                </div>
            </div>
        </div>

    </div>

</body>

</html>