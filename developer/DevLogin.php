<?php
session_start();
unset($_SESSION['username']);
session_destroy();

session_start();
include "../koneksi.php";

if (isset($_POST['prosesLogin'])) {

    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $username = $_POST['username'];
    $database = $_POST['database'];

    $result = mysqli_query($koneksi, "SELECT * FROM developer WHERE email = '$email'");
    $row = mysqli_fetch_assoc($result);

    if (
        strtolower($first_name) == $row['first_name'] &&
        strtolower($last_name) == $row['last_name'] &&
        strtolower($email) == $row['email'] &&
        $password == $row['password'] &&  //tidak dienkripsi karena tidak ada insert dan delete
        strtolower($username) == $row['username'] &&
        $database == $row['database_name']
    ) {
        $_SESSION['username'] = $_POST['username'];
        echo "
            <script>
            alert('Login Berhasil!...');
            document.location = 'DevChoice.php';
            </script>
            ";
    } else {
        echo "
            <script>
            alert('Login Gagal! Inputan Anda Salah...');
            document.location = 'DevLogin.php';
            </script>
            ";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/signPagess.css">
    <link rel="icon" href="../img/icon-dev.png">
    <title>MY DEV - Sign In</title>
</head>

<body>
    <div class="content">

        <div class="sign-up">
            <div class="form dev">
                <div class="center">
                    <h1>Welcome Back MY-DEV !</h1>
                    <hr>
                    <form action="DevLogin.php" method="POST">
                        <input type="text" name="first_name" placeholder="First Name" required>
                        <input type="text" name="last_name" placeholder="Last Name" required>
                        <input type="email" name="email" placeholder="E-mail" required>
                        <input type="password" name="password" placeholder="Password" required>
                        <input type="text" name="username" placeholder="Username" required>
                        <input type="text" name="database" placeholder="Database" required>
                        <button type="submit" name="prosesLogin">SIGN IN</button>
                    </form>
                </div>
            </div>
        </div>

    </div>

</body>

</html>