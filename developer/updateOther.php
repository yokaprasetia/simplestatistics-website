<?php
include "../koneksi.php";

$other_judul = $_GET['key'];

$result = mysqli_query($koneksi, "SELECT * FROM other WHERE other_judul = '$other_judul'");
$row = mysqli_fetch_assoc($result);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/update.css">
    <title>UPDATE</title>
</head>

<body>
    <div class="form">
        <h1>Other Form</h1>
        <form action="prosesUpdateOther.php" method="POST">
            <label for="href">Other Href</label>
            <input type="text" id="href" name="other_href" placeholder="Online Link" value="<?php echo $row['other_href']; ?>" readonly>
            <label for="judul">Other Judul</label>
            <input type="text" id="judul" name="other_judul" placeholder="Judul Konten Anda" value="<?php echo $row['other_judul']; ?>" required>
            <label for="img">Other Img</label>
            <input type="text" id="img" name="other_img" placeholder="materi/other/gambar/(nama.png) OR Link" value="<?php echo $row['other_img']; ?>" required>
            <ul>
                <li><button type="submit" name="update">Update</button></li>
                <li><button type="button" name="kembali" onclick="window.history.back()">Kembali</button></li>
            </ul>
        </form>
    </div>
</body>

</html>