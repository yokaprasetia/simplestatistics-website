<?php
    session_start();
    unset($_SESSION['user_id']);
    session_destroy();

    setcookie('id', '', time() - 3600);
    setcookie('key', '', time() - 3600);
    
    echo "
        <script>
        document.location = 'index.php';
        </script>
        ";
