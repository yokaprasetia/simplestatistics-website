<?php
include "../koneksi.php";
if (isset($_POST['prosesAdding'])) {

    $video_href = $_POST['video_href'];
    $id = $_POST['id'];
    $video_img = $_POST['video_img'];
    $video_judul = $_POST['video_judul'];

    $query = mysqli_query($koneksi, "SELECT * FROM video WHERE video_href = '$video_href'");
    $cek = mysqli_num_rows($query);

    // Error Handling
    if ($cek == 1) {
        echo "
            <script>
                alert('Gagal Menambah Data! Informasi telah digunakan...');
                document.location = 'DevVideo.php';
            </script>
            ";
    } else {
        mysqli_query($koneksi, "INSERT INTO video VALUES('$video_href', '$id', '$video_img', '$video_judul')");

        echo "
            <script>
                alert('Informasi berhasil ditambahkan! ');
                document.location = 'DevVideo.php';
                </script>
            ";
    }
}
?>


<?php
session_start();
if (isset($_SESSION['username'])) {
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/devCreate.css">
        <link rel="icon" href="../img/icon-dev.png">
        <title>MY DEV - Video Form</title>
    </head>

    <body>
        <div class="main">
            <div class="navbar">
                <ul>
                    <li><a href="DevMainMaterial.php">MAIN MATERIAL</a></li>
                    <li><a href="DevMateri.php">MATERI</a></li>
                    <li><a class="active" href="DevVideo.php">VIDEO</a></li>
                    <li><a href="DevReferensi.php">REFERENSI</a></li>
                    <li><a href="DevOther.php">OTHER</a></li>
                </ul>
            </div>
            <div class="form">
                <div class="border">
                    <h1>Video Form</h1>
                    <form action="DevVideo.php" method="POST">
                        <label for="href">Video Href</label>
                        <input type="text" id="href" name="video_href" placeholder="Online Link" required>
                        <label for="id">ID</label>
                        <input type="text" id="id" name="id" placeholder="Recent ID : anareg, statmat, oop, kmomstat, sig, pbw" required>
                        <label for="img">Video Img</label>
                        <input type="text" id="img" name="video_img" placeholder="materi/ (matkul) /video/ (file.png) OR Link" required>
                        <label for="judul">Video Judul</label>
                        <input type="text" id="judul" name="video_judul" placeholder="Judul dari Video Anda" required>
                        <input type="submit" value="ADD DATA" name="prosesAdding">
                    </form>
                </div>
            </div>
            <div class="logout">
                <div>
                    <a href="DevLogOut.php" onclick="return confirm('Apakah anda ingin Sign Out ?')"><img src="../img/dev-icon1.png"></a>
                    <p>Sign Out</p>
                </div>
            </div>
            <a class="back-to-menu" href="DevChoice.php">Back to Menu</a>
        </div>
    </body>

    </html>

<?php
} else {
    echo "
        <script>
            alert('Maaf! Login terlebih dahulu..');
            document.location = 'DevLogin.php';
        </script>
        ";
}
?>