<?php
    include "../../koneksi.php";
    
    session_start();
    if (isset($_SESSION['email'])) {
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Simple Statistic - Materi Pertama</title>
</head>
<body>
    <h1>Hello World</h1>
</body>
</html>

<?php
    } else {
        echo "
        <script>
        alert('Maaf, Sign In terlebih dahulu...');
        document.location = '../../SignInPage.php';
        </script>
        ";
    }
?>
