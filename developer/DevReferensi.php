<?php
include "../koneksi.php";
if (isset($_POST['prosesAdding'])) {

    $referensi_href = $_POST['referensi_href'];
    $id = $_POST['id'];
    $referensi_judul = $_POST['referensi_judul'];

    $query = mysqli_query($koneksi, "SELECT * FROM referensi WHERE referensi_href = '$referensi_judul'");
    $cek = mysqli_num_rows($query);

    // Error Handling
    if ($cek == 1) {
        echo "
            <script>
            alert('Gagal Menambah Data! Informasi telah digunakan...');
            document.location = 'DevReverensi.php';
            </script>
            ";
    } else {
        mysqli_query($koneksi, "INSERT INTO referensi VALUES('$referensi_href', '$id', '$referensi_judul')");

        echo "
            <script>
                alert('Informasi Berhasil Ditambahkan!');
                document.location = 'DevReferensi.php';
            </script>
            ";
    }
}
?>


<?php
session_start();
if (isset($_SESSION['username'])) {
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/devCreate.css">
        <link rel="icon" href="../img/icon-dev.png">
        <title>MY DEV - Referensi Form</title>
    </head>

    <body>
        <div class="main">
            <div class="navbar">
                <ul>
                    <li><a href="DevMainMaterial.php">MAIN MATERIAL</a></li>
                    <li><a href="DevMateri.php">MATERI</a></li>
                    <li><a href="DevVideo.php">VIDEO</a></li>
                    <li><a class="active" href="DevReferensi.php">REFERENSI</a></li>
                    <li><a href="DevOther.php">OTHER</a></li>
                </ul>
            </div>
            <div class="form">
                <div class="border">
                    <h1>Referensi Form</h1>
                    <form action="DevReferensi.php" method="POST">
                        <label for="href">Referensi Href</label>
                        <input type="text" id="href" name="referensi_href" placeholder="Online Link" required>
                        <label for="id">ID</label>
                        <input type="text" id="id" name="id" placeholder="Recent ID : anareg, statmat, oop, kmomstat, sig, pbw" required>
                        <label for="judul">Referensi Judul</label>
                        <input type="text" id="judul" name="referensi_judul" placeholder="Judul dari Referensi Anda" required>
                        <input type="submit" value="ADD DATA" name="prosesAdding">
                    </form>
                </div>
            </div>
            <div class="logout">
                <div>
                    <a href="DevLogOut.php" onclick="return confirm('Apakah anda ingin Sign Out ?')"><img src="../img/dev-icon1.png"></a>
                    <p>Sign Out</p>
                </div>
            </div>
            <a class="back-to-menu" href="DevChoice.php">Back to Menu</a>
        </div>
    </body>

    </html>

<?php
} else {
    echo "
        <script>
            alert('Maaf! Login terlebih dahulu..');
            document.location = 'DevLogin.php';
        </script>
        ";
}

?>