<?php
session_start();
include "../koneksi.php";

if (isset($_SESSION['username'])) {
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/devChoice.css">
        <title>MY DEV - Choice</title>
    </head>

    <body>
        <div class="main">
            <ul>
                <li><a href="DevMainMaterial.php">CREATE DATA</a></li>
                <li><a href="EditMainMaterial.php">EDIT DATA</a></li>
            </ul>
        </div>
    </body>

    </html>

<?php
} else {
    echo "
        <script>
            alert('Maaf! Login terlebih dahulu..');
            document.location = 'DevLogin.php';
        </script>
        ";
}
?>