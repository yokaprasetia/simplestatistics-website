<?php
include "../koneksi.php";

$main_judul = $_GET['key'];

$result = mysqli_query($koneksi, "SELECT * FROM main_material WHERE main_judul = '$main_judul'");
$row = mysqli_fetch_assoc($result);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/update.css">
    <title>UPDATE</title>
</head>

<body>
    <div class="form">
        <h1>Main Material Form</h1>
        <form action="prosesUpdateMainMaterial.php" method="POST">
            <label for="no">Nomor</label>
            <input type="text" id="no" name="no" placeholder="Recent No : 1, 2, 3, 4, 5, 6" value="<?php echo $row['no']; ?>" readonly>
            <label for="href">Main Href</label>
            <input type="text" id="href" name="main_href" placeholder="Recent : #anareg, #oop, #pbw, #komstat, #sig, #statmat" value="<?php echo $row['main_href']; ?>" required>
            <label for="id">ID</label>
            <input type="text" id="id" name="id" placeholder="Recent ID : anareg, statmat, oop, komstat, sig, pbw" value="<?php echo $row['id']; ?>" required>
            <label for="judul">Main Judul</label>
            <input type="text" id="judul" name="main_judul" placeholder="Recent : Analisis Regresi, Komputasi Statistik" value="<?php echo $row['main_judul']; ?>" required>
            <ul>
                <li><button type="submit" name="update">Update</button></li>
                <li><button type="button" name="kembali" onclick="window.history.back()">Kembali</button></li>
            </ul>

        </form>
    </div>
</body>

</html>