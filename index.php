<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/indexPages.css">
    <link rel="icon" href="img/all-faviconWeb.png">
    <title>Simple Statistik</title>
</head>

<body>
    <div class="navbar">
        <nav class="nav-atas">
            <div class="logo">
                <img src="img/all-animation1.png">
                <ul>
                    <li><a href="index.php">SIMPLE STATISTICS</a></li>
                </ul>
            </div>
            <ul class="menu">
                <li class="sign-up"><a href="SignUpPage.php">Sign Up</a></li>
            </ul>
        </nav>
    </div>
    <div class="space" id="space"></div>

    <div class="scroll">
        <a href="#space"><img class="scroll" src="img/all-scroller.png"></a>
        <p>Back to Top</p>
    </div>

    <div class="developer">
        <a href="developer/DevLogin.php"><img src="img/index-developer.png"></a>
        <p>Sign In</p>
    </div>

    <div class="main">

        <div class="c-satu">
            <div class="satu-judul">
                <h1>SELAMAT DATANG DI SIMPLE STATISTICS</h1>
                <p>"Upgrade Your Knowledge with Statistics"</p>
                <div class="link">
                    <a href="SignUpPage.php">Sign Up</a>
                </div>
            </div>
            <div class="satu-gambar">
                <img src="img/index-main1.png">
            </div>
        </div>

        <div class="c-dua">
            <div class="dua-judul">
                <img src="img/all-animation2.png">
                <h1>Mengapa Simple Statistics?</h1>
            </div>
            <div class="dua-gambar">
                <div>
                    <img src="img/index-icon1.png" alt="statistika">
                    <h2>SOFTWARE STATISTIKA</h2>
                    <p>Menyediakan materi pembelajaran terkait software yang membantu dalam menyelesaikan masalah di dunia statistik dengan cepat dan akurat</p>
                </div>
                <div>
                    <img src="img/index-icon2.png" alt="statistika">
                    <h2>DASAR STATISTIKA</h2>
                    <p>Menyediakan materi pembelajaran terkait Statistika yang dimulai dari dasar-dasar statistik hingga tahap lanjutan</p>
                </div>
                <div>
                    <img src="img/index-icon3.png" alt="statistika">
                    <h2>MATERI LAINNYA</h2>
                    <p>Terdapat materi yang diperlukan dan berkaitan dengan pentingnya ilmu di dunia statistik</p>
                </div>
            </div>
        </div>

        <div class="c-tiga">
            <div class="tiga-judul">
                <img src="img/all-animation2.png">
                <h1>Web Developer</h1>
            </div>
            <div class="tiga-gambar">
                <div>
                    <img src="img/index-rofi.png">
                    <p>MUHAMMAD ABDURROFI</p>
                </div>
                <div>
                    <img src="img/index-yoka.png">
                    <p>YOKA PRASETIA</p>
                </div>
            </div>
            <div class="tiga-kalimat">
                <div>
                    <h3>~ MENARIK ~</h3>
                    <hr>
                    <p>Web adalah sesuatu yang sangat menarik untuk dipelajari dimanapun dan kapanpun.</p>
                </div>
                <div>
                    <h3>~ INOVATIF ~</h3>
                    <hr>
                    <p>Menciptakan web dengan gaya yang inovatif agar memiliki daya tarik tersendiri.</p>
                </div>
                <div>
                    <h3>~ MANTAP ~</h3>
                    <hr>
                    <p>Hasil yang memuaskan adalah salah satu tujuan dari pembuatan web kami.</p>
                </div>
                <div>
                    <h3>~ KREATIF ~</h3>
                    <hr>
                    <p>Menciptakan hal hal baru yang jarang diketahui masyarakat umum.</p>
                </div>
            </div>
        </div>

        <div class="c-empat">
            <div class="empat-judul">
                <img src="img/all-animation2.png">
                <h1>Sponsored By</h1>
            </div>
            <div class="empat-gambar">
                <div>
                    <a href="http://simpledata.epizy.com/" target="_blank"><img src="img/index-support1.png"></a>
                    <p>SINIMASUK</p>
                </div>
                <div>
                    <a href="https://www.notion.so/Personal-Notes-a381d7e9e21e4d20aa5178702ab944c9" target="_blank"><img src="img/index-support2.png"></a>
                    <p>PERSONAL NOTES</p>
                </div>
                <div>
                    <a href="https://www.youtube.com/channel/UCihx8wK_5L18GVZOSHs-RHw" target="_blank"><img src="img/index-support3.png"></a>
                    <p>YOKA PRASETIA</p>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="footer-content">
                <div class="kiri">
                    <h3>Hubungi Kami</h3>
                    <p>WA (1) : <a href="https://wa.me/6282134523649" target="_blank">+62 821 3452 3649</a></p>
                    <p>WA (2) : <a href="https://wa.me/62895379261962" target="_blank">+62 8953 7926 1962</a></p>
                    <p>Online Setiap Hari</p>
                    <p>24 Jam Non-Stop</p>
                    <br>
                    <p>Jl. Otto Iskandardinata No.64C</p>
                    <p>Jakarta Timur, Jakarta</p>
                </div>
                <div class="kanan">
                    <h3>Connect With Us</h3>
                    <div class="sosmed">
                        <div>
                            <a href="https://facebook.com/yoka.prasetia.92" target="_blank"><img src="img/all-footer1.png"></a>
                            <p>Facebook</p>
                        </div>
                        <div>
                            <a href="https://www.youtube.com/channel/UCihx8wK_5L18GVZOSHs-RHw" target="_blank"><img src="img/all-footer2.png"></a>
                            <p>Youtube</p>
                        </div>
                        <div>
                            <a href="https://www.instagram.com/yokapras_mt" target="_blank"><img src="img/all-footer3.png"></a>
                            <p>Instagram</p>
                        </div>
                    </div>
                    <ul>
                        <li><a href="https://www.privacypolicyonline.com/live.php?token=ScPVc1KzbUlRlz1EppZyHcv8g7LWoQOt" target="_blank">Terms & Conditions</a></li>
                        <li><a href="https://www.privacypolicyonline.com/live.php?token=qRS6YqKdVm14EnetpZ1u5LqBcioyinxE" target="_blank">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer-logo">
                <p class="atas">SIMPLE STATISTICS</p>
                <p class="bawah">YOGYAKARTA&copy2021</p>
            </div>
            <div class="footer-end">
                <ul class="end">
                    <li><a href="ProgrammerPage.php" target="_blank">Programmer</a></li>
                    <li><a href="GammerPage.php" target="_blank">Gammer</a></li>
                    <li><a href="DataScientistPage.php" target="_blank">Data Scientist</a></li>
                    <li><a href="GraphicsDesignerPage.php" target="_blank">Graphics Designer</a></li>
                </ul>
            </div>
        </div>
    </div>

</body>

</html>