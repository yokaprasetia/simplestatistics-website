<?php
include "koneksi.php";

session_start();
if (isset($_SESSION['email'])) {
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/materialPagee.css">
        <link rel="icon" href="img/all-faviconWeb.png">
        <title>Simple Statistik - Material</title>
    </head>

    <body>
        <div class="navbar">
            <nav class="nav-atas">
                <div class="logo">
                    <img src="img/all-animation1.png">
                    <ul>
                        <li><a href="HomePage.php">SIMPLE STATISTICS</a></li>
                    </ul>
                </div>
                <ul class="menu">
                    <li><a href="HomePage.php">Home</a></li>
                    <li><a href="MaterialPage.php">Material</a></li>
                    <li><a href="OtherPage.php">Other</a></li>
                    <li class="sign-up"><a href="SignOutPage.php" onclick="return confirm('Apakah Anda yakin ingin melakukan Sign Out ?')">Sign Out</a></li>
                </ul>
            </nav>
        </div>
        <div class="space" id="space"></div>

        <div class="scroll">
            <a href="#space"><img class="scroll" src="img/all-scroller.png"></a>
            <p>Back to Top</p>
        </div>


        <div class="main">
            <div class="c-satu">
                <div class="satu-judul">
                    <h1>MAIN MATERIAL</h1>
                </div>

                <div class="satu-kalimat">
                    <?php
                    $data_item = mysqli_query($koneksi, "SELECT * FROM main_material");
                    while ($tampil = mysqli_fetch_array($data_item)) {
                    ?>
                        <a href="<?php echo $tampil['main_href']; ?>">
                            <p><?php echo $tampil['main_judul']; ?></p>
                        </a>
                    <?php
                    }
                    ?>
                </div>
            </div>

            <div class="c-dua">
                <div class="dua-judul">
                    <div>
                        <img src="img/all-animation3.png">
                        <h1>WE START OUR LESSON TODAY</h1>
                        <img src="img/all-animation4.png">
                    </div>
                    <p>"Keep Your Spirit and Never Give Up"</p>
                </div>
            </div>

            <?php
            $data_item = mysqli_query($koneksi, "SELECT * FROM main_material");
            $i = 1;
            $jumlah = 0;

            while ($tampil = mysqli_fetch_array($data_item)) {
                $jumlah++;
            }

            while ($i <= $jumlah) {
                $data_item = mysqli_query($koneksi, "SELECT * FROM materi INNER JOIN main_material ON materi.id = main_material.id WHERE main_material.no = '$i'");
                while ($tampil = mysqli_fetch_array($data_item)) {
            ?>

                    <div class="id" id="<?php echo $tampil['id']; ?>"></div>
                    <div class="materi">
                        <div class="content-judul">
                            <h1><?php echo $tampil['main_judul']; ?></h1>
                        </div>
                        <div class="content-materi">
                            <div class="judul">
                                <h2>MATERI</h2>
                            </div>
                            <ul>

                                <?php
                                $data_item = mysqli_query($koneksi, "SELECT * FROM materi INNER JOIN main_material ON materi.id = main_material.id WHERE main_material.no = '$i' LIMIT 0, 5");
                                while ($tampil = mysqli_fetch_array($data_item)) {
                                ?>
                                    <li><a href="<?php echo $tampil['materi_href']; ?>" target="_blank"><?php echo $tampil['materi_judul']; ?></a></li>
                                <?php
                                }
                                ?>

                            </ul>
                            <div class="lengkap">
                                <h3><a href="all_materi.php">Selengkapnya</a></h3>
                            </div>
                        </div>
                        <div class="content-video">
                            <div class="judul">
                                <h2>VIDEO TERKAIT</h2>
                            </div>
                            <div class="bungkus">

                                <?php
                                $data_item = mysqli_query($koneksi, "SELECT * FROM video INNER JOIN main_material ON video.id = main_material.id WHERE main_material.no = '$i' LIMIT 0, 8");
                                while ($tampil = mysqli_fetch_array($data_item)) {
                                ?>
                                    <div class="isi">
                                        <img src="<?php echo $tampil['video_img']; ?>">
                                        <hr>
                                        <p><?php echo $tampil['video_judul']; ?></p>
                                        <a href="<?php echo $tampil['video_href']; ?>" target="_blank">Klik di Sini</a>
                                    </div>
                                <?php
                                }
                                ?>

                            </div>
                            <div class="lengkap">
                                <h3><a href="all_video.php">Selengkapnya</a></h3>
                            </div>
                        </div>
                        <div class="content-other">
                            <div class="judul">
                                <h2>REFERENSI</h2>
                            </div>
                            <ul>

                                <?php
                                $data_item = mysqli_query($koneksi, "SELECT * FROM referensi INNER JOIN main_material ON referensi.id = main_material.id WHERE main_material.no = '$i' LIMIT 0, 3");
                                while ($tampil = mysqli_fetch_array($data_item)) {
                                ?>
                                    <li><a href="<?php echo $tampil['referensi_href']; ?>" target="blank"><?php echo $tampil['referensi_judul']; ?></a></li>
                                <?php
                                }
                                ?>

                            </ul>
                            <div class="lengkap">
                                <h3><a href="all_referensi.php">Selengkapnya</a></h3>
                            </div>
                        </div>
                    </div>

            <?php
                    $i++;
                }
            }
            ?>

            <div class="footer">
                <div class="footer-content">
                    <div class="kiri">
                        <h3>Hubungi Kami</h3>
                        <p>WA (1) : <a href="https://wa.me/6282134523649" target="_blank">+62 821 3452 3649</a></p>
                        <p>WA (2) : <a href="https://wa.me/62895379261962" target="_blank">+62 8953 7926 1962</a></p>
                        <p>Online Setiap Hari</p>
                        <p>24 Jam Non-Stop</p>
                        <br>
                        <p>Jl. Otto Iskandardinata No.64C</p>
                        <p>Jakarta Timur, Jakarta</p>
                    </div>
                    <div class="kanan">
                        <h3>Connect With Us</h3>
                        <div class="sosmed">
                            <div>
                                <a href="https://facebook.com/yoka.prasetia.92" target="_blank"><img src="img/all-footer1.png"></a>
                                <p>Facebook</p>
                            </div>
                            <div>
                                <a href="https://www.youtube.com/channel/UCihx8wK_5L18GVZOSHs-RHw" target="_blank"><img src="img/all-footer2.png"></a>
                                <p>Youtube</p>
                            </div>
                            <div>
                                <a href="https://www.instagram.com/yokapras_mt" target="_blank"><img src="img/all-footer3.png"></a>
                                <p>Instagram</p>
                            </div>
                        </div>
                        <ul>
                            <li><a href="https://www.privacypolicyonline.com/live.php?token=ScPVc1KzbUlRlz1EppZyHcv8g7LWoQOt" target="_blank">Terms & Conditions</a></li>
                            <li><a href="https://www.privacypolicyonline.com/live.php?token=qRS6YqKdVm14EnetpZ1u5LqBcioyinxE" target="_blank">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="footer-logo">
                    <p class="atas">SIMPLE STATISTICS</p>
                    <p class="bawah">YOGYAKARTA&copy2021</p>
                </div>
                <div class="footer-end">
                    <ul class="end">
                        <li><a href="ProgrammerPage.php" target="_blank">Programmer</a></li>
                        <li><a href="GammerPage.php" target="_blank">Gammer</a></li>
                        <li><a href="DataScientistPage.php" target="_blank">Data Scientist</a></li>
                        <li><a href="GraphicsDesignerPage.php" target="_blank">Graphics Designer</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </body>

    </html>

<?php
} else {
    echo "
        <script>
        alert('Maaf, Sign In terlebih dahulu...');
        document.location = 'SignInPage.php';
        </script>
        ";
}
?>