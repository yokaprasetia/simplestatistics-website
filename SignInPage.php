<?php
session_start();
unset($_SESSION['email']);
session_destroy();

session_start();
include "koneksi.php";

$error = false;

if (isset($_COOKIE['id']) && $_COOKIE['key']) {
    $id = $_COOKIE['id']; // password(hash)
    $key = $_COOKIE['key']; // email

    // ambil username
    $result = mysqli_query($koneksi, "SELECT password FROM user WHERE email = '$key'");
    $row = mysqli_fetch_assoc($result);

    if ($id === hash('sha256', $row['password'])) {
        $_SESSION['email'] = $key;
    }
}

if (isset($_SESSION['email'])) {
    header("Location: HomePage.php");
    exit;
}

if (isset($_POST['prosesSignIn'])) {

    $email = $_POST['email'];
    $password = $_POST['password'];

    $query = mysqli_query($koneksi, "SELECT * FROM user WHERE email = '$email'");
    $cek = mysqli_num_rows($query);

    //cek apakah terdapat data yang cocok di database
    if ($cek === 1) {
        $row = mysqli_fetch_assoc($query);
        if (password_verify($password, $row['password'])) {

            // cek remember me 
            if (isset($_POST['remember'])) {
                setcookie('id', hash('sha256', $row['password']), time() + 30 * 24 * 60 * 60);
                setcookie('key', $row['email'], time() + 30 * 24 * 60 * 60);
            }

            $_SESSION['email'] = $email;
            echo "
            <script>
            alert('Login Berhasil!...')
            document.location = 'HomePage.php';
            </script>
            ";
            exit;
        }
    }
    $error = true;
}

if ($error === true) {
    echo "
        <script>
        alert('Login Gagal! Email dan Password tidak sesuai...')
        document.location = 'SignInPage.php';
        </script>
        ";
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/signPagess.css">
    <link rel="icon" href="img/all-faviconWeb.png">
    <title>Simple Statistik - Sign In</title>
</head>

<body>
    <div class="content">

        <div class="sign-up">
            <div class="form">
                <div class="center">
                    <h1>Welcome Back Statistician!</h1>
                    <p class="sub">Sign In to See Your Growth</p>
                    <hr>
                    <form action="SignInPage.php" method="POST">
                        <input type="email" name="email" placeholder="E-mail" required>
                        <input type="password" name="password" placeholder="Password" required>
                        <div>
                            <input type="checkbox" id="remember" name="remember">
                            <label for="remember">Remember Me</label>
                        </div>
                        <button type="submit" name="prosesSignIn">Sign In</button>
                    </form>
                    <p class="link">Not a member? <a href="SignUpPage.php">Sign Up</a></p>
                </div>
            </div>
        </div>
    </div>

</body>

</html>