<?php
include "../koneksi.php";
if (isset($_POST['prosesAdding'])) {

    $no = $_POST['no'];
    $main_href = $_POST['main_href'];
    $id = $_POST['id'];
    $main_judul = $_POST['main_judul'];

    $query = mysqli_query($koneksi, "SELECT * FROM main_material WHERE no = '$no'");
    $cek = mysqli_num_rows($query);

    // Error Handling
    if ($cek == 1) {
        echo "
            <script>
            alert('Gagal Menambah Data! Informasi telah digunakan...');
            document.location = 'DevMainMaterial.php';
            </script>
            ";
    } else {
        mysqli_query($koneksi, "INSERT INTO main_material VALUES('$no', '$main_href', '$id', '$main_judul')");

        echo "
            <script>
                alert('Informasi Berhasil Ditambahkan! Namun, diperlukan isian pada materi tersebut!');
                document.location = 'DevMateri.php';
            </script>
            ";
    }
}
?>

<?php
session_start();
if (isset($_SESSION['username'])) {
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/editPage.css">
        <link rel="icon" href="../img/icon-dev.png">
        <title>MY DEV - Main Material Form</title>
    </head>

    <body>
        <div class="main" id="scroll">
            <div class="navbar">
                <ul>
                    <li><a href="EditMainMaterial.php">MAIN MATERIAL</a></li>
                    <li><a href="EditMateri.php">MATERI</a></li>
                    <li><a href="EditVideo.php">VIDEO</a></li>
                    <li><a href="EditReferensi.php">REFERENSI</a></li>
                    <li><a class="active" href="EditOther.php">OTHER</a></li>
                </ul>
            </div>

            <?php
            $result = mysqli_query($koneksi, "SELECT * FROM other");
            $rows = [];
            while ($row = mysqli_fetch_assoc($result)) {
                $rows[] = $row;
            }
            ?>


            <div class="main">

                <div class="scroll">
                    <a href="#scroll"><img class="scroll" src="../img/all-scroller.png"></a>
                    <p>Back to Top</p>
                </div>

                <div class="search">
                    <h3> KOLOM PENCARIAN</h3>
                    <input type="text" id="keyword" placeholder="Apa yang ingin Anda cari?">
                </div>

                <div class="content" id="container">
                    <ul>
                        <?php foreach ($rows as $row) : ?>
                            <li>
                                <p><?php echo $row['other_judul']; ?></p>
                                <a href="updateOther.php?key=<?php echo $row['other_judul']; ?>">Update</a>
                                <a href="deleteOther.php?key=<?php echo $row['other_judul']; ?>" onclick="return confirm('Apakah Anda yakin ingin menghapus <?php echo $row['other_judul']; ?> ?')">Delete</a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>



                <div class="logout">
                    <div>
                        <a href="DevLogOut.php" onclick="return confirm('Apakah anda ingin Sign Out ?')"><img src="../img/dev-icon1.png"></a>
                        <p>Sign Out</p>
                    </div>
                </div>
                <a class="back-to-menu" href="DevChoice.php">Back to Menu</a>
            </div>
            <script src="../javaScript/searchEditOther.js"></script>
    </body>

    </html>

<?php
} else {
    echo "
        <script>
            alert('Maaf! Login terlebih dahulu..');
            document.location = 'DevLogin.php';
        </script>
        ";
}

?>