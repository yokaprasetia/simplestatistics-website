<?php
include "../koneksi.php";
if (isset($_POST['prosesAdding'])) {

    $no = $_POST['no'];
    $main_href = $_POST['main_href'];
    $id = $_POST['id'];
    $main_judul = $_POST['main_judul'];

    $query = mysqli_query($koneksi, "SELECT * FROM main_material WHERE no = '$no'");
    $cek = mysqli_num_rows($query);

    // Error Handling
    if ($cek == 1) {
        echo "
            <script>
            alert('Gagal Menambah Data! Informasi telah digunakan...');
            document.location = 'DevMainMaterial.php';
            </script>
            ";
    } else {
        mysqli_query($koneksi, "INSERT INTO main_material VALUES('$no', '$main_href', '$id', '$main_judul')");

        echo "
            <script>
                alert('Informasi Berhasil Ditambahkan! Namun, diperlukan isian pada materi tersebut!');
                document.location = 'DevMateri.php';
            </script>
            ";
    }
}
?>

<?php
session_start();
if (isset($_SESSION['username'])) {
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/devCreate.css">
        <link rel="icon" href="../img/icon-dev.png">
        <title>MY DEV - Main Material Form</title>
    </head>

    <body>
        <div class="main">
            <div class="navbar">
                <ul>
                    <li><a class="active" href="DevMainMaterial.php">MAIN MATERIAL</a></li>
                    <li><a href="DevMateri.php">MATERI</a></li>
                    <li><a href="DevVideo.php">VIDEO</a></li>
                    <li><a href="DevReferensi.php">REFERENSI</a></li>
                    <li><a href="DevOther.php">OTHER</a></li>
                </ul>
            </div>
            <div class="form">
                <div class="border">
                    <h1>Main Material Form</h1>
                    <form action="DevMainMaterial.php" method="POST">
                        <label for="no">Nomor</label>
                        <input type="text" id="no" name="no" placeholder="Recent No : 1, 2, 3, 4, 5, 6" required>
                        <label for="href">Main Href</label>
                        <input type="text" id="href" name="main_href" placeholder="Recent : #anareg, #oop, #pbw, #komstat, #sig, #statmat" required>
                        <label for="id">ID</label>
                        <input type="text" id="id" name="id" placeholder="Recent ID : anareg, statmat, oop, kmomstat, sig, pbw" required>
                        <label for="judul">Main Judul</label>
                        <input type="text" id="judul" name="main_judul" placeholder="Recent : Analisis Regresi, Komputasi Statistik" required>
                        <input type="submit" value="ADD DATA" name="prosesAdding">
                    </form>
                </div>
            </div>
            <div class="logout">
                <div>
                    <a href="DevLogOut.php" onclick="return confirm('Apakah anda ingin Sign Out ?')"><img src="../img/dev-icon1.png"></a>
                    <p>Sign Out</p>
                </div>
            </div>
            <a class="back-to-menu" href="DevChoice.php">Back to Menu</a>
        </div>
    </body>

    </html>

<?php
} else {
    echo "
        <script>
            alert('Maaf! Login terlebih dahulu..');
            document.location = 'DevLogin.php';
        </script>
        ";
}

?>