<?php
include "../koneksi.php";
if (isset($_POST['prosesAdding'])) {

    $other_href = $_POST['other_href'];
    $other_judul = $_POST['other_judul'];
    $other_img = $_POST['other_img'];

    $query = mysqli_query($koneksi, "SELECT * FROM other WHERE other_href = '$other_href'");
    $cek = mysqli_num_rows($query);

    // Error Handling
    if ($cek == 1) {
        echo "
            <script>
            alert('Gagal Menambah Data! Informasi telah digunakan...');
            document.location = 'DevOther.php';
            </script>
            ";
    } else {
        mysqli_query($koneksi, "INSERT INTO other VALUES('$other_href', '$other_judul', '$other_img')");

        echo "
            <script>
                alert('Informasi Berhasil Ditambahkan!');
                document.location = 'DevOther.php';
            </script>
            ";
    }
}
?>


<?php
session_start();
if (isset($_SESSION['username'])) {
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/devCreate.css">
        <link rel="icon" href="../img/icon-dev.png">
        <title>MY DEV - Materi Form</title>
    </head>

    <body>
        <div class="main">
            <div class="navbar">
                <ul>
                    <li><a href="DevMainMaterial.php">MAIN MATERIAL</a></li>
                    <li><a href="DevMateri.php">MATERI</a></li>
                    <li><a href="DevVideo.php">VIDEO</a></li>
                    <li><a href="DevReferensi.php">REFERENSI</a></li>
                    <li><a class="active" href="DevOther.php">OTHER</a></li>
                </ul>
            </div>
            <div class="form">
                <div class="border">
                    <h1>Other Form</h1>
                    <form action="DevOther.php" method="POST">
                        <label for="href">Other Href</label>
                        <input type="text" id="href" name="other_href" placeholder="Online Link" required>
                        <label for="judul">Other Judul</label>
                        <input type="text" id="judul" name="other_judul" placeholder="Judul Konten Anda" required>
                        <label for="img">Other Img</label>
                        <input type="text" id="img" name="other_img" placeholder="materi/other/gambar/(nama.png) OR Link" required>
                        <input type="submit" value="ADD DATA" name="prosesAdding">
                    </form>
                </div>
            </div>
            <div class="logout">
                <div>
                    <a href="DevLogOut.php" onclick="return confirm('Apakah anda ingin Sign Out ?')"><img src="../img/dev-icon1.png"></a>
                    <p>Sign Out</p>
                </div>
            </div>
            <a class="back-to-menu" href="DevChoice.php">Back to Menu</a>
        </div>
    </body>

    </html>

<?php
} else {
    echo "
        <script>
            alert('Maaf! Login terlebih dahulu..');
            document.location = 'DevLogin.php';
        </script>
        ";
}

?>