<?php
include "koneksi.php";

session_start();
if (isset($_SESSION['email'])) {
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/homePagess.css">
        <link rel="icon" href="img/all-faviconWeb.png">
        <title>Simple Statistik - Home</title>
    </head>

    <body>
        <div class="navbar">
            <nav class="nav-atas">
                <div class="logo">
                    <img src="img/all-animation1.png">
                    <ul>
                        <li><a href="HomePage.php">SIMPLE STATISTICS</a></li>
                    </ul>
                </div>
                <ul class="menu">
                    <li><a href="HomePage.php">Home</a></li>
                    <li><a href="MaterialPage.php">Material</a></li>
                    <li><a href="OtherPage.php">Other</a></li>
                    <li class="sign-up"><a href="SignOutPage.php" onclick="return confirm('Apakah Anda yakin ingin melakukan Sign Out ?')">Sign Out</a></li>
                </ul>
            </nav>
        </div>
        <div class="space" id="space"></div>

        <div class="scroll">
            <a href="#space"><img class="scroll" src="img/all-scroller.png"></a>
            <p>Back to Top</p>
        </div>

        <div class="main">
            <div class="c-satu">
                <div class="satu-judul">
                    <img src="img/all-animation2.png">
                    <h1>Apa Itu Statistika?</h1>
                </div>
                <div class="satu-gambar">
                    <img src="img/homePage-materi1.png">
                    <p class="caption"><a href="https://www.pexels.com/id-id/foto/ilustrasi-grafik-hitam-biru-dan-merah-186461/" target="_blank">Pexels.com&copyBurak Kebapci</a></p>
                </div>
                <div class="satu-kalimat">
                    <p>Statistika adalah sebuah ilmu yang mempelajari bagaimana cara merencanakan, mengumpulkan, menganalisis, lalu menginterpretasikan, dan akhirnya mempresentasikan data. Singkatnya, statistika adalah ilmu yang bersangkutan dengan suatu data. Istilah "Statistika" berbeda dengan "Statistik". Statistika pada umumnya bekerja dengan memakai data numerik yang di mana adalah hasil cacahan maupun hasil pengkuran yang dilakukan dengan menggunakan data kategorik yang diklasifikasikan menurut sebuah kriteria tertentu.</p>
                    <p>Statistika merupakan ilmu yang berkaitan dengan data. Statistik adalah data itu sendiri, informasinya, atau hasil penerapan algoritme statistika pada suatu data tersebut. Dari kumpulan data, statistika dapat digunakan untuk menyimpulkan atau mendeskripsikan data; inilah yang dinamakan statistika deskriptif. Informasi kemudian dicatat sekaligus dikumpulkan baik itu dalam bentuk informasi numerik maupun informasi kategorik yang disebut sebagai suatu pengamatan. Sebagian besar konsep dasar statistika memberi asumsi mengenai teori probabilitas. Beberapa istilah statistika antara lain sebagai berikut: populasi, sampel, unit sampel, probabilitas.</p>
                    <p>Statistika juga telah banyak diterapkan dalam berbagai disiplin ilmu, baik itu ilmu-ilmu alam (misalnya astronomi dan biologi maupun ilmu-ilmu sosial (termasuk sosiologi dan psikologi), maupun di bidang bisnis (mengenai produk, dll), ekonomi, dan industri. Statistika juga digunakan dalam pemerintahan untuk mencapai berbagai macam tujuan; Sensus populasi masyarakat merupakan salah satu prosedur yang paling dikenal. Ada pula aplikasi statistika lain yang sekarang populer yaitu prosedur jajak pendapat atau polling (misalnya dilakukan sebelum pemilihan umum), serta hitung cepat (perhitungan cepat hasil pemilu) atau Quick count. Di bidang komputasi, statistika dapat pula diterapkan dalam pengenalan pola maupun kecerdasan buatan ("AI").</p>
                </div>
                <div class="clear"></div>
            </div>

            <div class="c-dua">
                <div class="dua-judul">
                    <img src="img/all-animation2.png">
                    <h1>Data Di Era 4.0</h1>
                </div>
                <div class="dua-kalimat">
                    <div>
                        <h4>VOLUME</h4>
                        <hr>
                        <p>Data yang dihasilkan setiap hari tumbuh secara eksponensial dan bukan hal yang aneh apabila ditemukan bisnis yang menghasilkan data berukuran ber-gigabyte setiap hari.</p>
                    </div>
                    <div>
                        <h4>VARIETY</h4>
                        <hr>
                        <p>Mengacu pada jenis data yang sangat beragam. Beberapa di antaranya dalam format teks dan angka. Lainnya dalam format seperti gambar, suara, dan video. Setiap jenis data ini membutuhkan metode analisis yang spesifik.</p>
                    </div>
                    <div>
                        <h4>VELOCITY</h4>
                        <hr>
                        <p>Mengacu pada jenis data yang dianggap sebagai data aliran.</p>
                    </div>
                    <div>
                        <h4>VERACITY</h4>
                        <hr>
                        <p>Mengacu pada situasi di mana informasi dalam data tersebar ‘tipis’ di seluruh data dan mereka hanya bermakna ketika digabungkan atau informasi yang relevan hanya terletak di sebagian kecil dari data yang tidak kita ketahui.</p>
                    </div>
                </div>
                <hr>
                <hr>
                <div class="dua-gambar">
                    <div>
                        <img src="img/homePage-data1.png">
                    </div>
                    <div>
                        <img src="img/homePage-data2.png">
                    </div>
                    <div>
                        <img src="img/homePage-data3.png">
                    </div>
                    <div>
                        <img src="img/homePage-data4.png">
                    </div>
                </div>
            </div>

            <div class="c-tiga">
                <div class="tiga-judul">
                    <div>
                        <img src="img/all-animation3.png">
                        <h1>WE ARE A STATISTICIAN</h1>
                        <img src="img/all-animation4.png">
                    </div>
                    <p>"TO INFINITY AND BEYOND WITH STATISTIC APPLICATION"</p>
                </div>
            </div>

            <div class="c-empat">
                <div class="empat-judul">
                    <img src="img/all-animation2.png">
                    <h1>Software Pendukung</h1>
                </div>
                <div class="empat-gambar">
                    <div>
                        <img src="img/homePage-icon01.png">
                        <p class="caption">HTML adalah singkatan dari Hypertext Markup Language. HTML memungkinkan seorang user untuk membuat dan menyusun bagian paragraf, heading, link atau tautan, dan blockquote untuk halaman web dan aplikasi.</p>
                    </div>
                    <div>
                        <img src="img/homePage-icon02.png">
                        <p class="caption">PHP adalah singkatan dari Hypertext Pre-processor, yang sebelumnya disebut Personal Home Pages. PHP Adalah bahasa scripting server-side. Bahasa ini digunakan untuk mengembangkan situs web statis atau situs web dinamis atau aplikasi Web.</p>
                    </div>
                    <div>
                        <img src="img/homePage-icon03.png">
                        <p class="caption">JavaScript adalah bahasa pemrograman yang digunakan dalam pengembangan website agar lebih dinamis dan interaktif disamping php sehingga dapat meningkatkan fungsionalitas pada halaman web.</p>
                    </div>
                    <div>
                        <img src="img/homePage-icon04.png">
                        <p class="caption">R adalah bahasa pemrograman dan perangkat lunak gratis yang dikembangkan oleh Ross Ihaka dan Robert Gentleman pada tahun 1993 yang memiliki banyak fungsi dan package untuk statistik dan visualisasi data yang lengkap.</p>
                    </div>
                    <div>
                        <img src="img/homePage-icon05.png">
                        <p class="caption">python adalah bahasa pemrograman tingkat tinggi, ditafsirkan, interaktif, dan berorientasi objek yang sangat populer di dunia.</p>
                    </div>
                    <div>
                        <img src="img/homePage-icon06.png">
                        <p class="caption">SPSS adalah aplikasi yang memiliki kemampuan analisis statistik cukup tinggi serta sistem manajemen data pada lingkungan grafis dengan menu deskriptif dan kotak yang sederhana sehingga mudah dipelajari.</p>
                    </div>
                </div>
            </div>

            <div class="footer">
                <div class="footer-content">
                    <div class="kiri">
                        <h3>Hubungi Kami</h3>
                        <p>WA (1) : <a href="https://wa.me/6282134523649" target="_blank">+62 821 3452 3649</a></p>
                        <p>WA (2) : <a href="https://wa.me/62895379261962" target="_blank">+62 8953 7926 1962</a></p>
                        <p>Online Setiap Hari</p>
                        <p>24 Jam Non-Stop</p>
                        <br>
                        <p>Jl. Otto Iskandardinata No.64C</p>
                        <p>Jakarta Timur, Jakarta</p>
                    </div>
                    <div class="kanan">
                        <h3>Connect With Us</h3>
                        <div class="sosmed">
                            <div>
                                <a href="https://facebook.com/yoka.prasetia.92" target="_blank"><img src="img/all-footer1.png"></a>
                                <p>Facebook</p>
                            </div>
                            <div>
                                <a href="https://www.youtube.com/channel/UCihx8wK_5L18GVZOSHs-RHw" target="_blank"><img src="img/all-footer2.png"></a>
                                <p>Youtube</p>
                            </div>
                            <div>
                                <a href="https://www.instagram.com/yokapras_mt" target="_blank"><img src="img/all-footer3.png"></a>
                                <p>Instagram</p>
                            </div>
                        </div>
                        <ul>
                            <li><a href="https://www.privacypolicyonline.com/live.php?token=ScPVc1KzbUlRlz1EppZyHcv8g7LWoQOt" target="_blank">Terms & Conditions</a></li>
                            <li><a href="https://www.privacypolicyonline.com/live.php?token=qRS6YqKdVm14EnetpZ1u5LqBcioyinxE" target="_blank">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="footer-logo">
                    <p class="atas">SIMPLE STATISTICS</p>
                    <p class="bawah">YOGYAKARTA&copy2021</p>
                </div>
                <div class="footer-end">
                    <ul class="end">
                        <li><a href="ProgrammerPage.php" target="_blank">Programmer</a></li>
                        <li><a href="GammerPage.php" target="_blank">Gammer</a></li>
                        <li><a href="DataScientistPage.php" target="_blank">Data Scientist</a></li>
                        <li><a href="GraphicsDesignerPage.php" target="_blank">Graphics Designer</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </body>

    </html>

<?php
} else {
    echo "
        <script>
        alert('Maaf, Sign In terlebih dahulu...');
        document.location = 'SignInPage.php';
        </script>
        ";
}
?>