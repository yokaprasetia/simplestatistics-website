<?php
include "../koneksi.php";

$video_judul = $_GET['key'];

$result = mysqli_query($koneksi, "SELECT * FROM video WHERE video_judul = '$video_judul'");
$row = mysqli_fetch_assoc($result);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/update.css">
    <title>UPDATE</title>
</head>

<body>
    <div class="form">
        <h1>video Form</h1>
        <form action="prosesUpdateVideo.php" method="POST">
            <label for="href">Video Href</label>
            <input type="text" id="href" name="video_href" placeholder="Online Link" value="<?php echo $row['video_href']; ?>" readonly>
            <label for="id">ID</label>
            <input type="text" id="id" name="id" placeholder="Recent ID : anareg, statmat, oop, kmomstat, sig, pbw" value="<?php echo $row['id']; ?>" required>
            <label for="img">Video Img</label>
            <input type="text" id="img" name="video_img" placeholder="materi/ (matkul) /video/ (file.png) OR Link" value="<?php echo $row['video_img']; ?>" required>
            <label for="judul">Video Judul</label>
            <input type="text" id="judul" name="video_judul" placeholder="Judul dari Video Anda" value="<?php echo $row['video_judul']; ?>" required>
            <ul>
                <li><button type="submit" name="update">Update</button></li>
                <li><button type="button" name="kembali" onclick="window.history.back()">Kembali</button></li>
            </ul>
        </form>
    </div>
</body>

</html>